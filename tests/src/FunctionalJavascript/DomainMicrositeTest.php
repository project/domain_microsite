<?php

namespace Drupal\Tests\domain_microsite\FunctionalJavascript;

use Drupal\domain_access\DomainAccessManagerInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\contextual\FunctionalJavascript\ContextualLinkClickTrait;
use Drupal\Tests\domain\Traits\DomainTestTrait;

/**
 * Tests domain microsite contextual links compatibility.
 */
class DomainMicrositeTest extends WebDriverTestBase {

  use ContextualLinkClickTrait;
  use DomainTestTrait;

  /**
   * Sets a base hostname for running tests.
   *
   * @var string
   */
  public $baseHostname;

  /**
   * Disabled config schema checking.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * We use the standard profile for testing.
   *
   * @var string
   */
  protected $profile = 'standard';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['block', 'contextual', 'domain_microsite', 'domain_access', 'field', 'node', 'user', 'system'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set the base hostname for domains.
    $this->baseHostname = \Drupal::entityTypeManager()->getStorage('domain')->createHostname();

    // Create default domain.
    $this->domainCreateTestDomains();
    $this->domains = $this->getDomains();
  }

  /**
   * Test domain microsite creation.
   */
  public function testDomainMicrosite() {
    $web_assert = $this->assertSession();
    $page = $this->getSession()->getPage();

    $admin = $this->drupalCreateUser([
      'access administration pages',
      'access contextual links',
      'administer blocks',
      'administer content types',
      'administer domains',
      'administer menu',
      'administer site configuration',
      'administer users',
      'bypass node access',
    ]);
    $this->drupalLogin($admin);

    // Must rebuild permissions.
    $this->drupalGet('admin/reports/status/rebuild');
    $page->pressButton('Rebuild permissions');

    // Test that form fields exist.
    $this->drupalGet('admin/config/domain/add');
    $web_assert->fieldExists('domain_microsite_is_domain_microsite');
    $web_assert->fieldExists('domain_microsite_parent_domain_id');
    $web_assert->fieldExists('domain_microsite_base_path');

    // Fill out form to create new domain microsite.
    $page->fillField('name', 'Domain Microsite 1');
    $page->checkField('domain_microsite_is_domain_microsite');
    $domain = current($this->domains);
    $page->fillField('domain_microsite_parent_domain_id', $domain->id());
    $page->fillField('domain_microsite_base_path', '/domain_microsite_1');
    $page->pressButton('Save');

    // Test that new domain microsite is successfully created and listed.
    $web_assert->pageTextContains($this->baseHostname.'/domain_microsite_1');

    // Test that new domain microsite is navigatable.
    $page->clickLink('http://'.$this->baseHostname.'/domain_microsite_1');
    $web_assert->addressEquals('domain_microsite_1');

    // Test that outbound path processor modified urls relative to domain microsite base path.
    $web_assert->elementAttributeContains('css', '.site-branding__logo', 'href', '/domain_microsite_1/');

    // Test contextual links path.
    $this->drupalGet('domain_microsite_1/');
    $web_assert->linkByHrefExists('/domain_microsite_1/admin/structure/menu/manage/main?destination=/domain_microsite_1');

    // Create a test node.
    $storage = \Drupal::entityTypeManager()->getStorage('domain');
    $domains = $storage->loadByProperties(['name' => 'Domain Microsite 1']);
    $domain_id = key($domains);
    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $node = $storage->create([
      'type' => 'page',
      'title' => 'Test node',
      'uid' => '1',
      'status' => 1,
      DomainAccessManagerInterface::DOMAIN_ACCESS_FIELD => [$domain_id],
      DomainAccessManagerInterface::DOMAIN_ACCESS_ALL_FIELD => 0,
    ]);
    $node->save();

    // Test node visibility.
    $this->drupalLogout();
    $this->drupalGet('domain_microsite_1/node/1');
    $web_assert->pageTextContains('Test node');
    $this->drupalGet('node/1');
    $web_assert->pageTextContains('Access denied');
  }

}
