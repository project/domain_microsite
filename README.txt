Domain Microsite by Path allows creating microsites at paths of existing domains/subdomains using the Domain module/ecosystem.


Installation:
Enable module as usual.
Then go to [admin/config/domain] to add a domain record.
If a default domain record doesn't already exist, create one first, then continue to create a domain microsite record.
Ignore hostname and machine_name because they will be overridden with auto-generated identifiers by this module.
Check "Make domain microsite" and enter values for "Parent domain" and "Base path".
Save to add a domain microsite by path.

Notes:
- Currently does no validation or checks against path aliases; microsite base path takes precedence.
