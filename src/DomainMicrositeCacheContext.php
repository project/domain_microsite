<?php

namespace Drupal\domain_microsite;

use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\domain\DomainNegotiatorInterface;

/**
 * Defines service for "per domain" caching.
 */
class DomainMicrositeCacheContext implements CacheContextInterface {

  /**
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  public function __construct(DomainNegotiatorInterface $domain_negotiator) {
    $this->domainNegotiator = $domain_negotiator;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Domain id');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    if ($domain = $this->domainNegotiator->getActiveDomain(TRUE)) {
      return $domain->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
