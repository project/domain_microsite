<?php

namespace Drupal\domain_microsite;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processes inbound and outbound paths for domain microsites.
 */
class DomainMicrositePathProcessor implements OutboundPathProcessorInterface, InboundPathProcessorInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $alias_manager;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * DomainMicrositePathProcessor constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\domain\DomainNegotiatorInterface $domain_negotiator
   *   The domain negotiator.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The path alias manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DomainNegotiatorInterface $domain_negotiator, AliasManagerInterface $alias_manager, ModuleHandlerInterface $module_handler) {
    $this->entityTypeManager = $entity_type_manager;
    $this->domainNegotiator = $domain_negotiator;
    $this->aliasManager = $alias_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    if ($active = $this->domainNegotiator->getActiveDomain()) {
      if ($base_path = $active->getThirdPartySetting('domain_microsite', 'base_path')) {
        if (strpos($path, $base_path) === 0) {
          $path = substr($path, strlen($base_path));
          if (!$path) {
            $path = '/';
          }
        }
      }
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = array(), Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if ($domain = $this->domainNegotiator->getActiveDomain()) {
      // Use the base_url option to change relative path to complete url for
      // links to entities that do not belong to the active domain because the
      // relative path might be inaccessible.
      if ($entity = $options['entity'] ?? NULL) {
        $entity_type = $entity->getEntityType();
        if ($entity_type instanceof \Drupal\Core\Entity\ContentEntityType && $entity->hasField('field_domain_access')) {
          $domain_id = $domain->id();
          $assigned_domains = array_column($entity->get('field_domain_access')->getValue(), 'target_id');
          $all_affiliates = $entity->get('field_domain_all_affiliates')->value;
          if (!$all_affiliates && $assigned_domains && !in_array($domain_id, $assigned_domains)) {
            // Use the first assigned domain because no better choice.
            if ($new_domain = $this->entityTypeManager->getStorage('domain')->load($assigned_domains[0])) {
              // This is the complete uri to the domain or domain microsite.
              $base_url = trim($new_domain->getPath(), '/');
              // Using base_url option may conflict with Language module.
              $options['base_url'] = $base_url;
              // Get the domain path alias on the new domain.
              // Domain_path 8.x-1.0 switching from path processors to path
              // manager alias override is why this is like this.
              if ($this->moduleHandler->moduleExists('domain_path')) {
                $source = $this->aliasManager->getPathByAlias($path);
                $this->domainNegotiator->setActiveDomain($new_domain);
                $path = $this->aliasManager->getAliasByPath($source);
                $this->domainNegotiator->setActiveDomain($domain);
              }
            }
          }
        }
      }
      // Append microsite base path if node is assigned to active domain.
      if (empty($new_domain) && $base_path = $domain->getThirdPartySetting('domain_microsite', 'base_path')) {
        $path = $base_path . $path;
      }
    }
    if ($bubbleable_metadata) {
      $bubbleable_metadata->addCacheContexts(['domain_microsite']);
    }
    return $path;
  }

}
