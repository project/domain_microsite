<?php

namespace Drupal\domain_microsite;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Provides trusted callback for render alters.
 *
 * @see domain_microsite_contextual_links_view_alter().
 */
class DomainMicrositeContextualLinksViewAlter implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['postRender'];
  }

  /**
   * Replaces contextual link hrefs with domain_microsite_base_path included.
   */
  public static function postRender($markup, array $element) {
    global $base_path;
    $key = key($element['#contextual_links']);
    $microsite_path = $element['#contextual_links'][$key]['metadata']['domain_microsite_base_path'] ?? '';
    if (!empty($microsite_path) && $microsite_path != '/') {
      $markup = str_replace('href="' . $base_path, 'href="' . $base_path . trim($microsite_path, '/') . '/', $markup);
    }
    return $markup;
  }

}
